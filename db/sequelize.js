const { Sequelize, DataTypes, Transaction } = require("sequelize");

const sequelize = new Sequelize("loanhood", "yudiz", "", {
  host: "localhost",
  dialect: "postgres",
});

try {
  sequelize.authenticate();
  console.log("Database connected");
} catch (error) {
  console.log("Database error " + error);
}

// (async () => await sequelize.sync())();

module.exports = { sequelize };
