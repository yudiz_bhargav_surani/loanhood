const Sequelize = require("sequelize");
const { sequelize } = require("../../db/sequelize");

const sizes = sequelize.define("sizes", {
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  sizegroupId: {
    type: Sequelize.BIGINT,
    references: {
      model: "sizegroups",
      key: "id",
    },
  },
  status: {
    type: Sequelize.BOOLEAN,
    defaultValue: true,
  },
});

sizes.sync();

module.exports = sizes;
