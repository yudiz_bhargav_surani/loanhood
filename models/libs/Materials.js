const Sequelize = require("sequelize");
const { sequelize } = require("../../db/sequelize");

const materials = sequelize.define("materials", {
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  status: {
    type: Sequelize.BOOLEAN,
    defaultValue: true,
  },
});

materials.sync();

module.exports = materials;
