const Sequelize = require("sequelize");
const { sequelize } = require("../../db/sequelize");

const User = sequelize.define("users", {
  firstName: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  lastName: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  userName: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true,
  },
  emailId: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true,
  },
  password: {
    type: Sequelize.STRING,
  },
  isEmailVerified: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  bio: {
    type: Sequelize.TEXT,
  },
  userInterests: {
    type: Sequelize.ARRAY(Sequelize.STRING),
  },
  isAdmin: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  isStripeVerified: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  isStripeMerchant: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  stripeCustomerId: {
    type: Sequelize.STRING,
  },
  stripeMerchantId: {
    type: Sequelize.STRING,
  },
  stripeIdentityId: {
    type: Sequelize.STRING,
  },
  avatarUrl: {
    type: Sequelize.STRING,
    defaultValue: process.env.DEFAULT_PROFILE_PIC,
  },
  neighbourhood: {
    type: Sequelize.STRING,
  },
  deliveryAddressId: {
    type: Sequelize.BIGINT,
  },
  dropOffAddressId: {
    type: Sequelize.BIGINT,
  },
  pickUpAddressId: {
    type: Sequelize.BIGINT,
  },
  userStatus: {
    type: Sequelize.STRING,
    defaultValue: "y",
  },
  token: {
    type: Sequelize.ARRAY(Sequelize.STRING),
  },
  rootSocket: {
    type: Sequelize.STRING,
  },
  deviceRegistrationToken: {
    type: Sequelize.ARRAY(Sequelize.STRING),
  },
  verificationToken: {
    type: Sequelize.STRING,
  },
  otp: {
    type: Sequelize.STRING,
  },
  isSocialLogin: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  isMarketingSubscribed: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  holidayStart: {
    type: Sequelize.DATEONLY,
  },
  holidayEnd: {
    type: Sequelize.DATEONLY,
  },
  isTest: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  isLoanHood: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  messagesLastSeenAt: {
    type: Sequelize.DATE,
  },
  notificationsLastSeenAt: {
    type: Sequelize.DATE,
  },
  shareUrl: {
    type: Sequelize.STRING,
  },
  aCode: {
    type: Sequelize.STRING,
  },
  person_id: {
    type: Sequelize.STRING,
  },
  hasNotificationOn: {
    type: Sequelize.BOOLEAN,
    defaultValue: true,
  },
});

User.sync();

module.exports = User;
