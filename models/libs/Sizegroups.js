const Sequelize = require("sequelize");
const { sequelize } = require("../../db/sequelize");

const sizegroups = sequelize.define("sizegroups", {
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  status: {
    type: Sequelize.BOOLEAN,
    defaultValue: true,
  },
});

sizegroups.sync();

module.exports = sizegroups;
