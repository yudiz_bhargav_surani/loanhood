const Address = require('./libs/Address');
const Brands = require('./libs/Brands');
const Categories = require('./libs/Categories');
const Colors = require('./libs/Colors');
const Follows = require('./libs/Follows');
const Likes = require('./libs/Likes');
const Materials = require('./libs/Materials');
const Messages = require('./libs/Messages');
const Rentalimages = require('./libs/Rentalimages');
const Rentalitems = require('./libs/Rentalitems');
const Rentals = require('./libs/Rentals');
const Rentaltransactions = require('./libs/Rentaltransactions');
const Rentaltransactionstates = require('./libs/Rentaltransactionstates');
const Reports = require('./libs/Reports');
const Sizegroups = require('./libs/Sizegroups');
const Sizes = require('./libs/Sizes');
const Subcategories = require('./libs/Subcategories');
const User = require('./libs/User');
const Bannerimages = require('./libs/Bannerimages');

// //users
// User.hasMany(Address, { foreignKey: "userId" });
// User.hasMany(Follows, { foreignKey: "userId" });
// User.hasMany(Follows, { foreignKey: "followerUserId" });
// User.hasMany(Likes, { foreignKey: "userId" });
// User.hasMany(Messages, { foreignKey: "senderId" });
// User.hasMany(Messages, { foreignKey: "receiverId" });
// User.hasMany(Rentals, { foreignKey: "userId" });
// User.hasMany(Rentaltransactions, { as: "loaner", foreignKey: "loanerId" });
// User.hasMany(Rentaltransactions, { as: "borrower", foreignKey: "borrowerId" });
// User.hasMany(Reports, { foreignKey: "userId" });
// User.hasMany(Reports, { foreignKey: "reportedUserId" });
// Address.belongsTo(User, { foreignKey: "userId" });
// Follows.belongsTo(User, { foreignKey: "userId" });
// Follows.belongsTo(User, { foreignKey: "followerUserId" });
// Likes.belongsTo(User, { foreignKey: "userId" });
// Messages.belongsTo(User, { foreignKey: "senderId" });
// Messages.belongsTo(User, { foreignKey: "receiverId" });
// Rentals.belongsTo(User, { foreignKey: "userId" });
// Rentaltransactions.belongsTo(User, { as: "loaner", foreignKey: "loanerId" });
// Rentaltransactions.belongsTo(User, { as: "borrower", foreignKey: "borrowerId" });
// Reports.belongsTo(User, { foreignKey: "userId" });
// Reports.belongsTo(User, { foreignKey: "reportedUserId" });

// //address
// Address.hasMany(Rentals, { foreignKey: "addressId" });
// Address.hasMany(Rentaltransactions, { foreignKey: "borrowerAddressId" });
// Address.hasMany(Rentaltransactions, { foreignKey: "loanerAddressId" });
// Rentals.belongsTo(Address, { foreignKey: "addressId" });
// Rentaltransactions.belongsTo(Address, { foreignKey: "borrowerAddressId" });
// Rentaltransactions.belongsTo(Address, { foreignKey: "loanerAddressId" });

// //rental
// Rentals.hasMany(Likes, { foreignKey: "rentalId" });
// Likes.belongsTo(Rentals, { foreignKey: "rentalId" });
// Rentals.hasMany(Messages, { foreignKey: "rentalId" });
// Messages.belongsTo(Rentals, { foreignKey: "rentalId" });
// Rentals.hasMany(Rentalimages, { foreignKey: "rentalId" });
// Rentalimages.belongsTo(Rentals, { foreignKey: "rentalId" });
// Rentals.hasMany(Rentalitems, { foreignKey: "rentalId" });
// Rentalitems.belongsTo(Rentals, { foreignKey: "rentalId" });
// Rentals.hasMany(Rentaltransactions, { foreignKey: "rentalId" });
// Rentaltransactions.belongsTo(Rentals, { foreignKey: "rentalId" });
// Rentals.hasMany(Reports, { foreignKey: "reportedRentalId" });
// Reports.belongsTo(Rentals, { foreignKey: "reportedRentalId" });

// //Rentaltransactions
// Rentaltransactions.hasMany(Messages, { foreignKey: "rentalTransactionsId" });
// Messages.belongsTo(Rentaltransactions, { foreignKey: "rentalTransactionsId" });
// Rentaltransactions.hasMany(Rentaltransactionstates, { foreignKey: "rentalTransactionsId" });
// Rentaltransactionstates.belongsTo(Rentaltransactions, { foreignKey: "rentalTransactionsId" });

// //Sizegroups
// Sizegroups.hasMany(Sizes, { foreignKey: "sizegroupId" });
// Sizes.belongsTo(Sizes, { foreignKey: "sizegroupId" });

// //Categories
// Categories.hasMany(Subcategories, { foreignKey: "categoryId" });
// Subcategories.belongsTo(Subcategories, { foreignKey: "categoryId" });
// Categories.hasMany(Rentalitems, { foreignKey: "categoryId" });
// Rentalitems.belongsTo(Categories, { foreignKey: "categoryId" });

// // Subcategories
// Subcategories.hasMany(Rentalitems, { foreignKey: "subcategoryId" });
// Rentalitems.belongsTo(Subcategories, { foreignKey: "subcategoryId" });

// //Brands
// Brands.hasMany(Rentalitems, { foreignKey: "brandId" });
// Rentalitems.belongsTo(Brands, { foreignKey: "brandId" });

// //color
// Colors.hasMany(Rentalitems, { foreignKey: "colorId" });
// Rentalitems.belongsTo(Colors, { foreignKey: "colorId" });

// //material
// Materials.hasMany(Rentalitems, { foreignKey: "materialId" });
// Rentalitems.belongsTo(Materials, { foreignKey: "materialId" });

//main
User.hasMany(Rentals, { onDelete: 'cascade' });
Rentals.belongsTo(User);

User.hasMany(Reports, { onDelete: 'cascade', foreignKey: 'userId', as: 'reporter' });
Reports.belongsTo(User, { foreignKey: 'userId', as: 'reporter' });

User.hasMany(Reports, { onDelete: 'cascade', foreignKey: 'reportedUserId', as: 'reported' });
Reports.belongsTo(User, { foreignKey: 'reportedUserId', as: 'reported' });

Rentals.hasMany(Rentalitems, { onDelete: 'cascade' });
Rentalitems.belongsTo(Rentals);

Rentals.hasMany(Reports, { onDelete: 'cascade', foreignKey: 'reportedRentalId' });
Reports.belongsTo(Rentals, { foreignKey: 'reportedRentalId' });

Rentals.hasMany(Rentalimages, { onDelete: 'cascade' });
Rentalimages.belongsTo(Rentals);

Sizes.hasMany(Rentalitems, { onDelete: 'cascade' });
Rentalitems.belongsTo(Sizes);

Sizegroups.hasMany(Sizes, { onDelete: 'cascade' });
Sizes.belongsTo(Sizegroups);

Categories.hasMany(Subcategories, { onDelete: 'cascade' });
Subcategories.belongsTo(Categories);

Subcategories.hasMany(Rentalitems, { onDelete: 'cascade' });
Rentalitems.belongsTo(Subcategories);

Categories.hasMany(Rentalitems, { onDelete: 'cascade' });
Rentalitems.belongsTo(Categories);

Colors.hasMany(Rentalitems, { onDelete: 'cascade' });
Rentalitems.belongsTo(Colors);

Brands.hasMany(Rentalitems, { onDelete: 'cascade' });
Rentalitems.belongsTo(Brands);

Materials.hasMany(Rentalitems, { onDelete: 'cascade' });
Rentalitems.belongsTo(Materials);

User.hasMany(Address, { onDelete: 'cascade' });
Address.belongsTo(User);

Address.hasMany(Rentals, { onDelete: 'cascade', foreignKey: 'addressId' });
Rentals.belongsTo(Address, { foreignKey: 'addressId' });

Rentals.hasMany(Rentaltransactions, { onDelete: 'cascade', foreignKey: 'rentalId' });
Rentaltransactions.belongsTo(Rentals, { foreignKey: 'rentalId' });

Rentals.hasMany(Messages, { onDelete: 'cascade' });
Messages.belongsTo(Rentals);

Rentaltransactions.hasMany(Messages, { onDelete: 'cascade', foreignKey: 'rentalTransactionId' });
Messages.belongsTo(Rentaltransactions, { foreignKey: 'rentalTransactionId' });

// Rentaltransactions.hasMany(Schedular, { onDelete: "cascade" });
// Schedular.belongsTo(Rentaltransactions);

Rentaltransactions.hasMany(Rentaltransactionstates, { onDelete: 'cascade' });
Rentaltransactionstates.belongsTo(Rentaltransactions);

User.hasMany(Likes, { onDelete: 'cascade' });
Likes.belongsTo(User);

Rentals.hasMany(Follows, { foreignKey: 'userId', sourceKey: 'userId' });
Follows.belongsTo(Rentals, { foreignKey: 'userId', sourceKey: 'userId' });

Rentals.hasMany(Reports, { foreignKey: 'userId', sourceKey: 'userId', as: 'reporterUser' });
Reports.belongsTo(Rentals, { foreignKey: 'userId', sourceKey: 'userId', as: 'reporterUser' });

Rentals.hasMany(Reports, { foreignKey: 'reportedUserId', sourceKey: 'userId', as: 'reportedUser' });
Reports.belongsTo(Rentals, { foreignKey: 'userId', as: 'reportedUser' });

Rentaltransactions.hasMany(Reports, { foreignKey: 'reportedUserId', sourceKey: 'loanerId', as: 'reportedLoaner' });
Reports.belongsTo(Rentaltransactions, { foreignKey: 'reportedUserId' });

Rentaltransactions.hasMany(Reports, { foreignKey: 'userId', sourceKey: 'loanerId', as: 'reporterLoaner' });
Reports.belongsTo(Rentaltransactions, { foreignKey: 'userId' });

Rentaltransactions.hasMany(Reports, { foreignKey: 'reportedUserId', sourceKey: 'borrowerId', as: 'reportedBorrower' });
Reports.belongsTo(Rentaltransactions, { foreignKey: 'reportedUserId' });

Rentaltransactions.hasMany(Reports, { foreignKey: 'userId', sourceKey: 'borrowerId', as: 'reporterBorrower' });
Reports.belongsTo(Rentaltransactions, { foreignKey: 'userId' });

Follows.hasMany(Reports, { foreignKey: 'userId', sourceKey: 'followerUserId', as: 'followingReported' });
Reports.belongsTo(Follows, { foreignKey: 'userId' });

Follows.hasMany(Reports, { foreignKey: 'reportedUserId', sourceKey: 'followerUserId', as: 'followingReporter' });
Reports.belongsTo(Follows, { foreignKey: 'reportedUserId' });

Follows.hasMany(Reports, { foreignKey: 'userId', sourceKey: 'userId', as: 'followerReported' });
Reports.belongsTo(Follows, { foreignKey: 'userId' });

Follows.hasMany(Reports, { foreignKey: 'reportedUserId', sourceKey: 'userId', as: 'followerReporter' });
Reports.belongsTo(Follows, { foreignKey: 'reportedUserId' });

Messages.hasMany(Reports, { foreignKey: 'reportedUserId', sourceKey: 'senderId', as: 'senderReport' });
Reports.belongsTo(Messages, { foreignKey: 'reportedUserId' });

Messages.hasMany(Reports, { foreignKey: 'reportedUserId', sourceKey: 'receiverId', as: 'receiverReport' });
Reports.belongsTo(Messages, { foreignKey: 'reportedUserId' });

Rentaltransactions.hasMany(Reports, { foreignKey: 'reportedUserId', sourceKey: 'loanerId', as: 'loanerReport' });
Reports.belongsTo(Rentaltransactions, { foreignKey: 'reportedUserId' });

Rentaltransactions.hasMany(Reports, { foreignKey: 'reportedUserId', sourceKey: 'borrowerId', as: 'borrowerReport' });
Reports.belongsTo(Rentaltransactions, { foreignKey: 'reportedUserId' });

Rentals.hasMany(Likes, { onDelete: 'cascade' });
Likes.belongsTo(Rentals);

User.hasMany(Rentaltransactions, { onDelete: 'cascade', foreignKey: 'borrowerId', as: 'borrower' });
Rentaltransactions.belongsTo(User, { foreignKey: 'borrowerId', as: 'borrower' });

User.hasMany(Rentaltransactions, { onDelete: 'cascade', foreignKey: 'loanerId', as: 'loaner' });
Rentaltransactions.belongsTo(User, { foreignKey: 'loanerId', as: 'loaner' });

Messages.hasMany(Follows, { foreignKey: 'userId', sourceKey: 'senderId', as: 'senderFollow' });
Follows.belongsTo(Messages, { foreignKey: 'userId', as: 'senderFollow' });

Messages.hasMany(Follows, { foreignKey: 'userId', sourceKey: 'receiverId', as: 'receiverFollow' });
Follows.belongsTo(Messages, { foreignKey: 'userId', as: 'receiverFollow' });

Address.hasMany(Rentaltransactions, { onDelete: 'cascade', foreignKey: 'borrowerAddressId', as: 'borrowerAddress' });
Rentaltransactions.belongsTo(Address, { foreignKey: 'borrowerAddressId', as: 'borrowerAddress' });

Address.hasMany(Rentaltransactions, { onDelete: 'cascade', foreignKey: 'loanerAddressId', as: 'loanerAddress' });
Rentaltransactions.belongsTo(Address, { foreignKey: 'loanerAddressId', as: 'loanerAddress' });

User.hasMany(Messages, { onDelete: 'cascade', foreignKey: 'senderId', as: 'sender' });
Messages.belongsTo(User, { foreignKey: 'senderId', as: 'sender' });

User.hasMany(Messages, { onDelete: 'cascade', foreignKey: 'receiverId', as: 'receiver' });
Messages.belongsTo(User, { foreignKey: 'receiverId', as: 'receiver' });

User.hasMany(Follows, { onDelete: 'cascade', foreignKey: 'userId', as: 'following' });
Follows.belongsTo(User, { foreignKey: 'userId', as: 'following' });

User.hasMany(Follows, { onDelete: 'cascade', foreignKey: 'followerUserId', as: 'follower' });
Follows.belongsTo(User, { foreignKey: 'followerUserId', as: 'follower' });

Messages.hasMany(Reports, { foreignKey: 'userId', sourceKey: 'receiverId' });
Reports.belongsTo(Messages, { foreignKey: 'userId', sourceKey: 'userId' });

Messages.hasMany(Reports, { foreignKey: 'reportedUserId', sourceKey: 'receiverId' });
Reports.belongsTo(Messages, { foreignKey: 'userId' });

module.exports = {
  User,
  Brands,
  Categories,
  Colors,
  Materials,
  Sizegroups,
  Sizes,
  Address,
  Follows,
  Rentals,
  Subcategories,
  Likes,
  Rentalimages,
  Rentalitems,
  Rentaltransactions,
  Rentaltransactionstates,
  Reports,
  Messages,
  Bannerimages,
};
