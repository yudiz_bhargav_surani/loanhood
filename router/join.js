const router = require('express').Router();
const controller = require('../controllers/join');

router.get('/rentalImagesLoaner', controller.rentalImagesLoaner);
router.get('/singleRentalItemImagesLoaner', controller.singleRentalItemImagesLoaner);
router.get('/rentalTransactionsLoanerBorrower', controller.rentalTransactionsLoanerBorrower);
router.get('/rentalLoanerRentalTransaction', controller.rentalLoanerRentalTransaction);
router.get('/userWithReportedUser', controller.userWithReportedUser);
router.get('/rentalWithReportedUser', controller.rentalWithReportedUser);
router.get('/rentalWithTransactionAndState', controller.rentalWithTransactionAndState);
router.get('/rentalWithMetadata', controller.rentalWithMetadata);
router.get('/rentalWhoLoanerAndBorrower', controller.rentalWhoLoanerAndBorrower);
router.get('/chatMessageWithLoggedIn', controller.chatMessageWithLoggedIn);
router.get('/chatMessageOfLoanerAndBorrower', controller.chatMessageOfLoanerAndBorrower);
router.get('/metaDataOfRentalWithRental', controller.metaDataOfRentalWithRental);
router.get('/likesOfRentalWithUser', controller.likesOfRentalWithUser);
router.get('/followerWithRentalImages', controller.followerWithRentalImages);
router.get('/followingWithRentalImages', controller.followingWithRentalImages);
router.get('/userReportedByUser', controller.userReportedByUser);
router.get('/userReportedByUserWithReason', controller.userReportedByUserWithReason);
router.get('/userWhoReportedRental', controller.userWhoReportedRental);
router.get('/userWhoBlockedYouWithRental', controller.userWhoBlockedYouWithRental);
router.get('/userWhoYouBlockedYouWithRental', controller.userWhoYouBlockedYouWithRental);

module.exports = router;
