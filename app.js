const express = require("express");
const app = express();
const bodyParser = require("body-parser");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
require("dotenv").config();
require("./db/sequelize");
// require("./models");

const routes = require("./router/routes");

app.use("/", routes);

app.listen(3000, () => console.log(`app running on port 3000!`));
