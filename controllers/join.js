const {
  User,
  Brands,
  Categories,
  Colors,
  Materials,
  Sizegroups,
  Sizes,
  Address,
  Follows,
  Rentals,
  Subcategories,
  Likes,
  Rentalimages,
  Rentalitems,
  Rentaltransactions,
  Rentaltransactionstates,
  Reports,
  Messages,
  Bannerimages,
} = require('../models/index');

const { Sequelize, Op, fn, col } = require('sequelize');
const controller = {};

// 1 Need a list of rentals with their rental images and with their loaner.
controller.rentalImagesLoaner = async (req, res) => {
  try {
    const result = await Rentals.findAll({
      include: [
        { model: Rentalimages, where: { status: true } },
        { model: User, required: true, attributes: ['id', 'firstName', 'userName'] },
      ],
    });
    return res.status(200).json({ result });
  } catch (error) {
    return res.status(501).json(error.message);
  }
};

//2 Get a single Rental with rental items, images and with their Loaner.
controller.singleRentalItemImagesLoaner = async (req, res) => {
  try {
    const result = await Rentals.findAll({
      where: { id: 6 },
      include: [
        { model: Rentalitems },
        { model: Rentalimages },
        { model: User, required: true, attributes: ['id', 'firstName', 'userName'] },
      ],
    });
    return res.status(200).json({ result });
  } catch (error) {
    return res.status(501).json(error.message);
  }
};

// 3 Need a list of rentals with their rental transactions, their loaner and borrower.
controller.rentalTransactionsLoanerBorrower = async (req, res) => {
  try {
    const result = await Rentals.findAll({
      include: [
        {
          model: Rentaltransactions,
          include: [
            { model: User, required: true, as: 'loaner', attributes: ['id', 'firstName', 'userName'] },
            { model: User, required: true, as: 'borrower', attributes: ['id', 'firstName', 'userName'] },
          ],
        },
      ],
    });
    return res.status(200).json({ result });
  } catch (error) {
    return res.status(501).json(error.message);
  }
};

// 4 Get a rental with their loaner and their rental transactions.
controller.rentalLoanerRentalTransaction = async (req, res) => {
  try {
    const result = await Rentals.findOne({
      where: { id: 6 },
      include: [{ model: User, required: true }, { model: Rentaltransactions }],
    });
    return res.status(200).json({ result });
  } catch (error) {
    return res.status(501).json(error.message);
  }
};

// 5 Need a list of users with their reported users.
controller.userWithReportedUser = async (req, res) => {
  try {
    const result = await User.findAll({
      attributes: ['id', 'firstName', 'lastName'],
      include: [
        {
          model: Reports,
          required: true,
          as: 'reporter',
          include: [{ model: User, as: 'reported', attributes: ['id', 'firstName', 'userName'] }],
        },
      ],
    });
    return res.status(200).json({ result });
  } catch (error) {
    return res.status(501).json(error.message);
  }
};

// 6 Need a list of rentals with the reported user
controller.rentalWithReportedUser = async (req, res) => {
  try {
    const result = await Rentals.findAll({
      include: [
        { model: Reports, include: [{ model: User, as: 'reported', attributes: ['id', 'firstName', 'userName'] }] },
      ],
    });
    return res.status(200).json({ result });
  } catch (error) {
    return res.status(501).json(error.message);
  }
};

// 7 Need a list of rentals with their rental transactions and their rental transactions states.
controller.rentalWithTransactionAndState = async (req, res) => {
  try {
    const result = await Rentals.findAll({
      include: [{ model: Rentaltransactions, include: [{ model: Rentaltransactionstates }] }],
    });
    return res.status(200).json({ result });
  } catch (error) {
    return res.status(501).json(error.message);
  }
};

// 8 Get a rental with their meta data (brands, categories, subcategories, size etc.)
controller.rentalWithMetadata = async (req, res) => {
  try {
    const result = await Rentals.findOne({
      include: [
        {
          model: Rentalitems,
          include: [{ model: Brands }, { model: Categories }, { model: Subcategories }, { model: Sizes }],
        },
      ],
    });
    return res.status(200).json({ result });
  } catch (error) {
    return res.status(501).json(error.message);
  }
};

// 9 Get all the details about a rental like who is the loaner, how many borrowers with their details, price, what is the rental transaction state, what is the latest state.

controller.rentalWhoLoanerAndBorrower = async (req, res) => {
  try {
    const resultCount = await Rentals.findOne({
      where: { id: 4 },
      include: [{ model: Rentaltransactions, required: true, attributes: [] }],
      attributes: [[fn('COUNT', col('rentalTransactions.id')), 'borrowerCount']],
      group: ['rentals.id'],
    });

    const result = await Rentals.findOne({
      where: { id: 4 },
      include: [
        { model: User, attributes: ['id', 'firstName', 'userName'] },
        {
          model: Rentaltransactions,
          required: true,
          attributes: ['id', 'loanerId', 'borrowerId', 'currentStatus'],
          include: [{ model: User, as: 'borrower', attributes: ['id', 'firstName', 'userName'] }],
        },
      ],

      attributes: ['id', 'userId', 'title', 'originalPrice', 'rentalPricePerWeek'],
    });
    return res.status(200).json({ resultCount, result });
  } catch (error) {
    return res.status(500).json(error.message);
  }
};

// 10 Need a list of chat messages with all the loaner/borrower considering you are the logged in user and getting the chat dashboard with the latest message.
controller.chatMessageWithLoggedIn = async (req, res) => {
  try {
    const result = await Rentaltransactions.findAndCountAll({
      attributes: ['id', 'loanerId', 'borrowerId'],
      where: { [Op.or]: [{ loanerId: 39 }, { borrowerId: 39 }] },
      include: [
        {
          model: Messages,
          required: true,
          order: [[Messages, 'createdAt', 'DESC']],
          attributes: ['id', 'rentalTransactionId', 'senderId', 'receiverId', 'message', 'createdAt', 'updatedAt'],
          where: { key: 'chat' },
          include: [
            { model: User, required: true, as: 'sender', attributes: ['id', 'firstName', 'userName'] },
            { model: User, required: true, as: 'receiver', attributes: ['id', 'firstName', 'userName'] },
          ],
        },
      ],
    });
    return res.status(200).json({ result });
  } catch (error) {
    return res.status(500).json(error.message);
  }
};

// 11 Need chat messages of a particular chat between loaner and a borrower with the detail of loaner borrower and the rental.
controller.chatMessageOfLoanerAndBorrower = async (req, res) => {
  try {
    const result = await Messages.findAll({
      required: true,
      where: { key: 'chat', [Op.or]: [{ senderId: 39 }, { receiverId: 39 }] },
      // order: [[Messages, 'createdAt', 'DESC']],
      attributes: ['id', 'rentalTransactionId', 'senderId', 'receiverId', 'message', 'createdAt', 'updatedAt'],
      include: [
        {
          model: Rentaltransactions,
          attributes: ['id', 'rentalId', 'loanerId', 'borrowerId'],
          include: [
            { model: User, required: true, as: 'loaner', attributes: ['id', 'firstName', 'userName'] },
            { model: User, required: true, as: 'borrower', attributes: ['id', 'firstName', 'userName'] },
            { model: Rentals, attributes: ['id', 'userId', 'title'] },
          ],
        },
      ],
    });
    return res.status(200).json({ result });
  } catch (error) {
    return res.status(500).json(error.message);
  }
};

// 12 Need only meta data of the rental with the rental itself.
controller.metaDataOfRentalWithRental = async (req, res) => {
  try {
    const result = await Rentals.findOne({
      where: { id: 4 },
      attributes: ['id', 'userId', 'title'],
      include: [
        {
          model: Rentalitems,
          include: [{ model: Brands }, { model: Categories }, { model: Subcategories }, { model: Sizes }],
        },
      ],
    });
    return res.status(200).json({ result });
  } catch (error) {
    return res.status(500).json(error.message);
  }
};

// 13 Need a list of likes of a rental with the users who liked it.
controller.likesOfRentalWithUser = async (req, res) => {
  try {
    const result = await Rentals.findAll({
      attributes: ['id', 'userId', 'title'],
      include: [{ model: Likes, include: [{ model: User, attributes: ['id', 'firstName', 'lastName', 'userName'] }] }],
    });
    return res.status(200).json({ result });
  } catch (error) {
    return res.status(500).json(error.message);
  }
};

// 14 Need a list of followers with their rentals, images and rental items, and the blocked users and who blocked you users won’t be shown in the list
controller.followerWithRentalImages = async (req, res) => {
  try {
    const result = await User.findAll({
      where: { id: 4 },
      attributes: ['id', 'firstName', 'userName'],
      include: [
        { model: Follows, as: 'follower' },
        {
          model: Rentals,
          attributes: ['id', 'userId', 'title'],
          include: [{ model: Rentalimages }, { model: Rentalitems }],
        },
        {
          model: Reports,
          as: 'reporter',
          where: { isBlocked: true },
          include: [{ model: User, as: 'reported', attributes: ['id', 'firstName', 'userName'] }],
        },
      ],
    });
    return res.status(200).json({ result });
  } catch (error) {
    return res.status(500).json(error.message);
  }
};

// 14a Need a list of following with their rentals, images and rental items, and the blocked users and who blocked you users won’t be shown in the list
controller.followingWithRentalImages = async (req, res) => {
  try {
    const result = await User.findAll({
      where: { id: 4 },
      attributes: ['id', 'firstName', 'userName'],
      include: [
        { model: Follows, as: 'following' },
        {
          model: Rentals,
          attributes: ['id', 'userId', 'title'],
          include: [{ model: Rentalimages }, { model: Rentalitems }],
        },
        {
          model: Reports,
          as: 'reporter',
          where: { isBlocked: true },
          include: [{ model: User, as: 'reported', attributes: ['id', 'firstName', 'userName'] }],
        },
      ],
    });
    return res.status(200).json({ result });
  } catch (error) {
    return res.status(500).json(error.message);
  }
};

// 15 Need a list of users reported by a particular user(you) considering you are the logged in user.
controller.userReportedByUser = async (req, res) => {
  try {
    const result = await User.findAll({
      attributes: ['id', 'firstName', 'lastName'],
      include: [{ model: Reports, as: 'reporter' }],
    });
    return res.status(200).json({ result });
  } catch (error) {
    return res.status(500).json(error.message);
  }
};

// 16 Need a list of users who reported you with the reason.
controller.userReportedByUserWithReason = async (req, res) => {
  try {
    const result = await User.findAll({
      attributes: ['id', 'firstName', 'lastName'],
      include: [
        {
          model: Reports,
          as: 'reporter',
          include: [{ model: User, as: 'reported', attributes: ['id', 'firstName', 'lastName'] }],
        },
      ],
    });
    return res.status(200).json({ result });
  } catch (error) {
    return res.status(500).json(error.message);
  }
};

// 17 Need a list of user who reported the rental.
controller.userWhoReportedRental = async (req, res) => {
  try {
    const result = await User.findAll({
      attributes: ['id', 'firstName', 'lastName'],
      include: [
        {
          model: Reports,
          as: 'reported',
          include: [{ model: User, as: 'reporter', attributes: ['id', 'firstName', 'lastName'] }],
        },
      ],
    });
    return res.status(200).json({ result });
  } catch (error) {
    return res.status(500).json(error.message);
  }
};

// 18 Need a list of users who blocked you with the user’s rental.
controller.userWhoBlockedYouWithRental = async (req, res) => {
  try {
    const result = await User.findAll({
      attributes: ['id', 'firstName', 'lastName'],
      include: [
        {
          model: Reports,
          as: 'reporter',
          where: { isBlocked: true },
          include: [
            { model: User, as: 'reported', attributes: ['id', 'firstName', 'lastName'] },
            { model: Rentals, attributes: ['id', 'userId', 'title'] },
          ],
        },
      ],
    });
    return res.status(200).json({ result });
  } catch (error) {
    return res.status(500).json(error.message);
  }
};

// 19 Need a list of users who you blocked with the user’s rental;.
controller.userWhoYouBlockedYouWithRental = async (req, res) => {
  try {
    const result = await User.findAll({
      attributes: ['id', 'firstName', 'lastName'],
      include: [
        {
          model: Reports,
          as: 'reported',
          where: { isBlocked: true },
          include: [
            { model: User, as: 'reporter', attributes: ['id', 'firstName', 'lastName'] },
            { model: Rentals, attributes: ['id', 'userId', 'title'] },
          ],
        },
      ],
    });
    return res.status(200).json({ result });
  } catch (error) {
    return res.status(500).json(error.message);
  }
};

module.exports = controller;
